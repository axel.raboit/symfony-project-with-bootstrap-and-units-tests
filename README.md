# Projet Symfony avec plusieurs entités dont chacunes sont testé avec des units tests
# Front effectué avec Webpack Encore + Sass + Bootstrap de configuré
# Connecté sur Gitlab avec une Pipeline


## ENTITIES

### User
-> id [OK]
-> firstname (varchar_255, null) [OK]
-> lastname (varchar_255, null) [OK]
-> pseudo (varchar_255, not null) [OK]
-> password (not null) [OK]
-> blogs (realted to the blog entity) [OK]
-> posts (related to the post entity) [OK]
-> comments (related to the comment entity) [OK]
-> createdAt (not null)

## Blog
-> id
-> name (varchar_255, not null) [OK]
-> description (text, null) [OK]
-> user (related to the user entity) [OK]
-> posts (related to the post entity) [OK]
-> createdAt (not null)
-> updatedAt (null)

## Post
-> id
-> title (varchar_255, not null) [OK]
-> description (text, not null) [OK]
-> content (text, not null) [OK]
-> author (related to the user entity) [OK]
-> blog (related to the blog entity) [OK]
-> category (related to the category entity) [OK] if the category was deleted we have ti reassign the category's post
-> comments (related to the comment entity) [OK]
-> createdAt (not null)
-> updatedAt (null)

## Category
-> id
-> name (varchar_255, not null) [OK]
-> description (text, null) [OK]
-> posts (related to the post entity) [OK] 

## Comment
-> id
-> content (text, not null) [OK]
-> author (related to the user entity) [OK] if a comment has no more author we have to specify that the comment is from unknown
-> post (related to the post entity) [OK]
-> createdAt (not null)
-> updatedAt (null)

## Installation in the project
- Webpack encore
- Webpack notifier
- Bootstrap
- Sass
- Sass Loader
- core js
- poppers js
- stimulus
- stimulus bridge








