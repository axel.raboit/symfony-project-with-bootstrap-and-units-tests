// assets/app.js
// (or assets/bootstrap.js - and then import it from app.js)
import 'bootstrap/dist/js/bootstrap.bundle';
import { startStimulusApp } from '@symfony/stimulus-bridge';

export const app = startStimulusApp(require.context(
    '@symfony/stimulus-bridge/lazy-controller-loader!./controllers',
    true,
    /\.(j|t)sx?$/
));