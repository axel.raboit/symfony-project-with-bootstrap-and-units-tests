<?php

namespace App\Tests;

use App\Entity\Blog;
use App\Entity\Post;
use App\Entity\User;
use App\Entity\Category;
use App\Entity\Comment;
use PHPUnit\Framework\TestCase;

class PostUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $post = new Post();
        $category = new Category();
        $author = new User();
        $blog = new Blog();

        $post->setTitle('title');
        $post->setDescription('description');
        $post->setContent('content');
        $post->setCategory($category);
        $post->setAuthor($author);
        $post->setBlog($blog);

        $this->assertTrue($post->getTitle() === 'title');
        $this->assertTrue($post->getDescription() === 'description');
        $this->assertTrue($post->getContent() === 'content');
        $this->assertTrue($post->getCategory() === $category);
        $this->assertTrue($post->getAuthor() === $author);
        $this->assertTrue($post->getBlog() === $blog);
    }

    public function testIsFalse(): void
    {
        $post = new Post();
        $category = new Category();
        $author = new User();
        $blog = new Blog();

        $post->setTitle('title');
        $post->setDescription('description');
        $post->setContent('content');
        $post->setCategory($category);
        $post->setAuthor($author);
        $post->setBlog($blog);

        $this->assertFalse($post->getTitle() === 'false');
        $this->assertFalse($post->getDescription() === 'false');
        $this->assertFalse($post->getContent() === 'false');
        $this->assertFalse($post->getCategory() === new Category());
        $this->assertFalse($post->getAuthor() === new User());
        $this->assertFalse($post->getBlog() === new Blog());
    }

    public function testIsEmpty(): void
    {
        $post = new Post();

        $this->assertEmpty($post->getTitle());
        $this->assertEmpty($post->getDescription());
        $this->assertEmpty($post->getContent());
        $this->assertEmpty($post->getCategory());
        $this->assertEmpty($post->getAuthor());
        $this->assertEmpty($post->getBlog());
    }
}
