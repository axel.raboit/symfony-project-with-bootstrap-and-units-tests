<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new User();

        $user->setEmail('test@gmail.com');
        $user->setPassword('password');
        $user->setFirstname('firstname');
        $user->setLastname('lastname');
        $user->setPseudo('pseudo');

        $this->assertTrue($user->getEmail() === 'test@gmail.com');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getFirstname() === 'firstname');
        $this->assertTrue($user->getLastname() === 'lastname');
        $this->assertTrue($user->getPseudo() === 'pseudo');
    }

    public function testIsFalse(): void
    {
        $user = new User();

        $user->setEmail('test@gmail.com');
        $user->setPassword('password');
        $user->setFirstname('firstname');
        $user->setLastname('lastname');
        $user->setPseudo('pseudo');

        $this->assertFalse($user->getEmail() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getFirstname() === 'false');
        $this->assertFalse($user->getLastname() === 'false');
        $this->assertFalse($user->getPseudo() === 'false');
    }

    public function testIsEmpty(): void
    {
        $user = new User();
        $user->setPassword('');

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getFirstname());
        $this->assertEmpty($user->getLastname());
        $this->assertEmpty($user->getPseudo());
    }
}
