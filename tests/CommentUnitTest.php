<?php

namespace App\Tests;

use App\Entity\Comment;
use PHPUnit\Framework\TestCase;

class CommentUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $comment = new Comment();

        $comment->setContent('content');

        $this->assertTrue($comment->getContent() === 'content');
    }

    public function testIsFalse(): void
    {
        $comment = new Comment();

        $comment->setContent('content');

        $this->assertFalse($comment->getContent() === 'false');
    }

    public function testIsEmpty(): void
    {
        $comment = new Comment();

        $this->assertEmpty($comment->getContent());
    }
}
